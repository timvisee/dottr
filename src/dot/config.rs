use std::collections::HashMap;
use std::fs::File;
use std::path::Path;

use serde::Deserialize;
use serde_yaml;

use super::Dot;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub dots: HashMap<String, Dot>,
}

impl Config {
    /// Load a configuration from the given file path.
    // TODO: add error type
    pub fn load_file<P: AsRef<Path>>(path: P) -> Result<Self, ()> {
        // Open the configuration file, parse and return
        let file = File::open(path).expect("failed to open configuration file");
        Ok(serde_yaml::from_reader(file).expect("failed to parse configuration"))
    }

    /// Load a configuration from the given path.
    ///
    /// If this is an directory, the configuration file path is inferred.
    // TODO: add error type
    pub fn load_path<P: AsRef<Path>>(path: P) -> Result<Self, ()> {
        // Append dottr configuration file if not in path
        if path.as_ref().is_dir() {
            path.as_ref().join("dot.yml");
        }

        Self::load_file(path)
    }
}
