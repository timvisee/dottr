use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::dot::{Config, Dot};

/// A profile which specifies what dots are enabled and disabled.
#[derive(Debug, Serialize, Deserialize)]
pub struct Profile {
    /// List of profiles, enabled or disabled.
    pub dots: HashMap<String, bool>,
}

impl Profile {
    /// Build a profile for the given configuration, with the given enabled decider closure.
    pub fn from_with<F>(config: &Config, enabled: F) -> Self
    where
        F: Fn(&Dot) -> bool,
    {
        Self {
            dots: config
                .dots
                .iter()
                .map(|(k, v)| (k.clone(), enabled(v)))
                .collect(),
        }
    }
}
