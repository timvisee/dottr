pub mod generate;
pub mod install;
pub mod uninstall;

// Re-export to cmd module
pub use self::generate::CmdGenerate;
pub use self::install::CmdInstall;
pub use self::uninstall::CmdUninstall;
