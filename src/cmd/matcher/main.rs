use std::path::PathBuf;

use clap::ArgMatches;

use super::Matcher;
use crate::cmd::arg::{ArgConfig, CmdArgOption};
use crate::util::env_var_present;

/// The main command matcher.
pub struct MainMatcher<'a> {
    matches: &'a ArgMatches<'a>,
}

impl<'a: 'b, 'b> MainMatcher<'a> {
    /// Get the configuration file to use.
    pub fn config(&self) -> PathBuf {
        ArgConfig::value(self.matches)
    }

    /// Check whether to force.
    pub fn force(&self) -> bool {
        self.matches.is_present("force") || env_var_present("DOTTR_FORCE")
    }

    /// Check whether to use no-interact mode.
    pub fn no_interact(&self) -> bool {
        self.matches.is_present("no-interact") || env_var_present("DOTTR_NO_INTERACT")
    }

    /// Check whether to assume yes.
    pub fn assume_yes(&self) -> bool {
        self.matches.is_present("yes") || env_var_present("DOTTR_YES")
    }

    /// Check whether quiet mode is used.
    pub fn quiet(&self) -> bool {
        !self.verbose() && (self.matches.is_present("quiet") || env_var_present("DOTTR_QUIET"))
    }

    /// Check whether verbose mode is used.
    pub fn verbose(&self) -> bool {
        self.matches.is_present("verbose") || env_var_present("DOTTR_VERBOSE")
    }
}

impl<'a> Matcher<'a> for MainMatcher<'a> {
    fn with(matches: &'a ArgMatches) -> Option<Self> {
        Some(MainMatcher { matches })
    }
}
