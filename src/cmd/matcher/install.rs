use clap::ArgMatches;

use super::Matcher;

/// The install command matcher.
pub struct InstallMatcher<'a> {
    matches: &'a ArgMatches<'a>,
}

impl<'a: 'b, 'b> InstallMatcher<'a> {}

impl<'a> Matcher<'a> for InstallMatcher<'a> {
    fn with(matches: &'a ArgMatches) -> Option<Self> {
        matches
            .subcommand_matches("install")
            .map(|matches| Self { matches })
    }
}
