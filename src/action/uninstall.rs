use clap::ArgMatches;
use failure::Fail;

use crate::cmd::matcher::{MainMatcher, Matcher, UninstallMatcher};

/// A file uninstall action.
pub struct Uninstall<'a> {
    cmd_matches: &'a ArgMatches<'a>,
}

impl<'a> Uninstall<'a> {
    /// Construct a new uninstall action.
    pub fn new(cmd_matches: &'a ArgMatches<'a>) -> Self {
        Self { cmd_matches }
    }

    /// Invoke the uninstall action.
    // TODO: create a trait for this method
    pub fn invoke(&self) -> Result<(), Error> {
        // Create the command matchers
        let matcher_main = MainMatcher::with(self.cmd_matches).unwrap();
        let matcher_uninstall = UninstallMatcher::with(self.cmd_matches).unwrap();

        // TODO: load configuration
        println!("TODO: run uninstallation process here");

        Ok(())
    }
}

#[derive(Debug, Fail)]
pub enum Error {
    /// An error occurred while uninstalling dotfiles.
    #[fail(display = "")]
    Uninstall,
}
