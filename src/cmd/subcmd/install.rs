use clap::{App, SubCommand};

/// The install command definition.
pub struct CmdInstall;

impl CmdInstall {
    pub fn build<'a, 'b>() -> App<'a, 'b> {
        // Build the subcommand
        SubCommand::with_name("install")
            .about("Install dotfiles")
            .visible_alias("i")
    }
}
