use std::path::PathBuf;

use serde::Deserialize;

use super::Link;

#[derive(Debug, Deserialize)]
pub struct Dot {
    /// Optional custom dot name
    name: Option<String>,

    /// Optional custom dot base path
    path: Option<PathBuf>,

    /// File links.
    links: Vec<Link>,
}
